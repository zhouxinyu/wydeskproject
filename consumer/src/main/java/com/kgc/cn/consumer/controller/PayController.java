package com.kgc.cn.consumer.controller;

import com.alibaba.fastjson.JSONObject;
import com.kgc.cn.common.enums.PayEnum;
import com.kgc.cn.common.model.dto.Goodorder;
import com.kgc.cn.common.model.dto.User;
import com.kgc.cn.common.model.dto.Weixinuser;
import com.kgc.cn.common.returnResult.ReturnResult;
import com.kgc.cn.common.returnResult.ReturnResultUtils;
import com.kgc.cn.consumer.config.aop.CurrentUser;
import com.kgc.cn.consumer.config.aop.LoginRequired;
import com.kgc.cn.consumer.paramModel.goodIdSetParam;
import com.kgc.cn.consumer.service.PayConsumerService;
import com.kgc.cn.consumer.utils.active.ActiveMQUtils;
import com.kgc.cn.consumer.utils.redis.RedisUtils;
import com.kgc.cn.consumer.utils.wxPay.WxUrl;
import com.kgc.cn.consumer.utils.wxPay.WxUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @auther zhouxinyu
 * @data 2019/12/23
 */
@Api(tags = "微信支付")
@Controller
@RequestMapping(value = "/pay")
public class PayController {
    @Autowired
    private PayConsumerService payConsumerService;

    /**
     * 去支付
     *
     * @param goodIdSet
     * @return
     * @throws Exception
     */

    @ApiOperation(value = "去支付")
    @PostMapping(value = "/WxPay")
    @ResponseBody
    @LoginRequired
    public ReturnResult WxPay(@CurrentUser Object o,
                              @Valid goodIdSetParam goodIdSet) throws Exception {
        String objectJsonStr = JSONObject.toJSONString(o);
        String phone = null;
        //如果字符串里包含openid，说明当前登录的是微信用户
        if (objectJsonStr.contains("openId")) {
            Weixinuser weixinuser = JSONObject.parseObject(objectJsonStr, Weixinuser.class);
            phone = weixinuser.getPhone();
            if (null == phone) {
                return ReturnResultUtils.returnFail(PayEnum.PAY_NOPHONE);
            }
        } else {
            User user = JSONObject.parseObject(objectJsonStr, User.class);
            phone = user.getPhone();
        }
        // 生成未付款订单
        List<Goodorder> list = payConsumerService.creatNoPayOrder(phone, goodIdSet.getShippingId(), goodIdSet.getGoodIdSet());
        // 再调用微信支付方法进行微信支付
        String code = payConsumerService.WxPayCode(list);
        // 用code生成二维码
        return ReturnResultUtils.returnSuccess(payConsumerService.codeToPicture(code));

    }

    @Autowired
    private WxUrl wxUrl;
    @Autowired
    private ActiveMQUtils activeMQUtils;
    @Autowired
    private RedisUtils redisUtils;

    /**
     * 微信支付回调
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/WxIsPay")
    @ApiIgnore
    public String WxIsPay(HttpServletRequest request, HttpServletResponse response) throws Exception {
     if(redisUtils.checkSpeed("WxIsPay",1,86580)){
         // 定义流
         InputStream inputStream = request.getInputStream();
         BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
         StringBuffer stringBuffer = new StringBuffer();
         String line = "";
         while (null != (line = bufferedReader.readLine())) {
             stringBuffer.append(line);
         }
         bufferedReader.close();
         inputStream.close();
         // 将xml转换成map
         Map<String, String> map = WxUtil.xmlToMap(stringBuffer.toString());
         // 判断签名是否正确
         boolean isSign = WxUtil.isSignatureValid(map, wxUrl.getKey());
         if (isSign) {
             // 签名正确
             if ("SUCCESS".equals(map.get("return_code"))) {
                 //获取各参数进行业务处理
                 String orderId = map.get("out_trade_no");
                 // 得到实付款
                 BigDecimal price = new BigDecimal(map.get("total_fee"));
                 String endTime = map.get("end_time");
                 // 得到订单中所有的商品
                 List<Goodorder> goodorder = payConsumerService.selectgoodByOrderId(orderId);
                 int amounts = goodorder.stream().mapToInt(goodorder1 -> Integer.parseInt(goodorder1.getShopMoney())).sum();
                 // 得到应付款
                 BigDecimal amount = new BigDecimal(amounts);

                 // 订单不存在
                 if (CollectionUtils.isEmpty(goodorder)) {
                     System.out.println("订单不存在");
                     response.setContentType("text/xml;charset=utf-8");
                     response.getWriter().write("<xml><return_code><![CDATA[FAIL]]></return_code><return_msg><![CDATA[订单不存在]]></return_msg></xml>");
                 } else {
                     // 订单存在
                     // 判断应付款和实付款是不是一样
                     if (price.compareTo(amount) != 0) {
                         // 不一样
                         System.out.println("资金不足，请换一种支付方式");
                         response.setContentType("text/xml;charset=utf-8");
                         response.getWriter().write("<xml><return_code><![CDATA[FAIL]]></return_code><return_msg><![CDATA[资金不足]]></return_msg></xml>");
                     } else {
                         // 一样
                         System.out.println("付款成功！");
                         // 修改库中订单状态为已支付
                         activeMQUtils.sendMsgByQueue("paySuccess", orderId);
                         //打印一下已完成
                         System.out.println("交易完成！");
                         response.setContentType("text/xml;charset=utf-8");
                         response.getWriter().write("<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>");
                         return "redirect:" + "/前端地址/?flag = true";
                     }

                 }

             } else {
                 System.out.println("签名错误！");
                 response.setContentType("text/xml;charset=utf-8");
                 response.getWriter().write("<xml><return_code><![CDATA[FAIL]]></return_code><return_msg><![CDATA[签名错误]]></return_msg></xml>");
             }
         }
         return "redirect:" + "/前端地址/?flag = true";
     }
     return "重复回调";
    }

    /**
     * 监听订单状态更改和商品库存更改
     *
     * @param orderId
     */

    @JmsListener(destination = "paySuccess")
    public void payListener(String orderId) {
        // 更新订单状态
        payConsumerService.updateToOrdered(orderId);
        // 更新库存
        payConsumerService.updateNum(orderId);
    }
}

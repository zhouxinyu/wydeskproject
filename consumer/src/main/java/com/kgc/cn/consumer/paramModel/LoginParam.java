package com.kgc.cn.consumer.paramModel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Ding on 2019/12/24.
 */
@Data
@Builder
@ApiModel("登录model")
@AllArgsConstructor
@NoArgsConstructor
public class LoginParam {
    @ApiModelProperty(value = "手机号")
    private String phone;
    @ApiModelProperty(value = "验证码")
    private String code;
}

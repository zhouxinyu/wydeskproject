package com.kgc.cn.consumer.service.Impl;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Sets;
import com.kgc.cn.common.model.dto.Goodorder;
import com.kgc.cn.common.model.dto.User;
import com.kgc.cn.common.model.dto.Weixinuser;
import com.kgc.cn.common.utils.Date.DateUtils;
import com.kgc.cn.consumer.service.LoginAndRegisterService;
import com.kgc.cn.consumer.service.MemberService;
import com.kgc.cn.consumer.service.PayConsumerService;
import com.kgc.cn.consumer.utils.redis.RedisUtils;
import com.kgc.cn.consumer.utils.wxPay.WxPay;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by Ding on 2019/12/23.
 */
@Service
public class MemberServiceImpl implements MemberService {
    @Autowired
    private RedisUtils redisUtils;
    @Autowired
    private LoginAndRegisterService loginAndRegisterService;
    @Autowired
    private WxPay wxPay;
    @Autowired
    private PayConsumerService payConsumerService;

    @Override
    public String addMember(Object o, int addMonth) throws Exception {
        String objectJsonStr = JSONObject.toJSONString(o);
        //如果字符串里包含openid，说明当前登录的是微信用户
        if (objectJsonStr.contains("openId")) {
            Weixinuser weixinuser = JSONObject.parseObject(objectJsonStr, Weixinuser.class);
            Weixinuser mySqlWxUser = loginAndRegisterService.queryUserByOpenId(weixinuser.getOpenId());
            String phone = mySqlWxUser.getPhone();
            if (null != phone) {
                //调用支付方法
                if (true) {
                    User user = loginAndRegisterService.queryUserByPhone(phone);
                    String key = "member:" + phone;
                    //调用购买或者续费会员方法
                    updateMemberTime(user, key, addMonth);
                    return "0";
                }
                return "202";
            }
            return "201";
        } else {
            User user = JSONObject.parseObject(objectJsonStr, User.class);
            String phone = user.getPhone();
            //调用支付方法
            if (true) {
                String key = "member:" + phone;
                //调用购买或者续费会员方法
                updateMemberTime(user, key, addMonth);
                return "0";
            }
            return "202";
        }
    }


    /**
     * 购买或者续费会员
     *
     * @param user
     * @param key
     * @param addMonth
     */
    private void updateMemberTime(User user, String key, int addMonth) {
        //获取当前时间加上月份
        Date date = DateUtils.getNewDate(addMonth);
        //判断当前账户是否是会员（购买或者续费）
        if (!redisUtils.hasKey(key)) {
            //不存在则添加会员到期时间
            String userJsonStr = JSONObject.toJSONString(user);
            redisUtils.set(key, userJsonStr);
            //设置redis的key固定日期失效，时间小于当前日期立即失效
            redisUtils.expireAt(key, date);
        } else {
            // 存在则续费会员
            Long expireTime = redisUtils.pttl(key);
            Long times = System.currentTimeMillis();
            Long addTime = (date.getTime() - times)/1000;
            redisUtils.expire(key, expireTime + addTime);
        }
    }
}

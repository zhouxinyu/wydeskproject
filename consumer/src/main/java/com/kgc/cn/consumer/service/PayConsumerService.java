package com.kgc.cn.consumer.service;

import com.kgc.cn.common.model.dto.Goodorder;

import java.util.List;
import java.util.Set;

/**
 * @auther zhouxinyu
 * @data 2019/12/23
 */
public interface PayConsumerService {

    // 生成未支付订单
    List<Goodorder> creatNoPayOrder(String phone, int shippingId, Set<String> goodIdSet) throws Exception;

    //生成二维码所需要的code
    String WxPayCode(List<Goodorder> list) throws Exception;

    // code转二维码
    String codeToPicture(String code) throws Exception;

    // 查找订单所有商品
    List<Goodorder> selectgoodByOrderId(String orderId);

    //更新订单状态
    int updateToOrdered(String orderId);

    //修改good的库存
    int updateNum(String orderId);

}

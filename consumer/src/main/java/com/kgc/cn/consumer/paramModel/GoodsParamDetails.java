package com.kgc.cn.consumer.paramModel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@ApiModel("商品详情model")
@AllArgsConstructor
@NoArgsConstructor
public class GoodsParamDetails implements Serializable {
    private static final long serialVersionUID = 3477027467955151935L;
    //名字 原价 实付价格 图片 描述
    @ApiModelProperty(value = "商品名称",example = "1")
    private String goodName;
    @ApiModelProperty(value = "原价",example = "1")
    private int goodPrice;
    @ApiModelProperty(value = "实付价格",example = "1")
    private double sellMoney;
    @ApiModelProperty(value = "图片",example = "1")
    private String pictureSource;
    @ApiModelProperty(value = "描述",example = "1")
    private String goodContent;

}

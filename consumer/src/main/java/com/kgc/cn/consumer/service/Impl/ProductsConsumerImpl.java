package com.kgc.cn.consumer.service.Impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.google.common.collect.Lists;
import com.kgc.cn.common.model.dto.Goods;
import com.kgc.cn.common.service.ProductsCommonService;
import com.kgc.cn.consumer.paramModel.GoodsParam;
import com.kgc.cn.consumer.paramModel.GoodsParamDetails;
import com.kgc.cn.consumer.paramModel.PageBeanParam;
import com.kgc.cn.consumer.service.ProductsConsumerService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class ProductsConsumerImpl implements ProductsConsumerService {

    @Reference
    private ProductsCommonService productsCommon;

    /**
     * 商品列表要按类型来展示商品列表（从后台中获取商品列表）
     *
     * @param typeId
     * @return
     */
    @Override
    public PageBeanParam<GoodsParam> ProductList(int typeId, int current, int size) throws Exception{

        //从数据库查询
        List<Goods> goodsList = productsCommon.ProductList(typeId, current, size);
        if (CollectionUtils.isEmpty(goodsList)) {
            return null;
        } else {
            List<GoodsParam> goodsParamList = Lists.newArrayList();
            goodsList.forEach(goods -> {
                GoodsParam goodsParam = GoodsParam.builder().build();
                BeanUtils.copyProperties(goods, goodsParam);
                goodsParamList.add(goodsParam);
            });
            PageBeanParam<GoodsParam> pageBeanParam = PageBeanParam.<GoodsParam>builder()
                    .current(current)
                    .size(size)
                    .count(queryCount(typeId))
                    .data(goodsParamList)
                    .build();
            return pageBeanParam;
        }
    }

    /**
     * 查询该类型商品总数量
     * 用于分页
     *
     * @return
     */
    private int queryCount(int typeId) {
        return productsCommon.queryCount(typeId);
    }

    /**
     * 商品详情（要体现 名字 原价 实付价格 图片 描述）
     *
     * @param goodId
     * @return
     */
    @Override
    public GoodsParamDetails ProductsDetails(String goodId) throws Exception {
        if (null != productsCommon.ProductsDetails(goodId)) {

            //传值：名字 原价 折扣 图片 描述
            Goods goods = productsCommon.ProductsDetails(goodId);
            //int整数类型 double双精度类型
            double discount = (double) productsCommon.DiscountParam(goodId) / 100;

            GoodsParamDetails goodsParamDetails = GoodsParamDetails.builder()
                    .goodName(goods.getGoodName())
                    .goodPrice(goods.getGoodPrice())
                    .pictureSource(goods.getPictureSource())
                    .goodContent(goods.getGoodContent())
                    .sellMoney(goods.getGoodPrice() * discount)
                    .build();

            return goodsParamDetails;

        } else {
            return null;
        }
    }

    /**
     * 返回商品库存
     *
     * @param goodId
     * @return
     */

    public int ProductsNum(String goodId) {
        return productsCommon.ProductsNum(goodId);
    }
}

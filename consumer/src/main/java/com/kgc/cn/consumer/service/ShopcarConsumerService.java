package com.kgc.cn.consumer.service;

import com.kgc.cn.consumer.paramModel.ShopcarInfoParam;

import javax.servlet.http.HttpServletRequest;
import java.net.UnknownHostException;
import java.util.List;

/**
 * Created by boot on 2019/12/24
 */
public interface ShopcarConsumerService {

    // 添加购物车
    String addShopcar(String goodId, int shopNum, HttpServletRequest request) throws UnknownHostException;

    // 显示购物车列表
    List showShopcar(HttpServletRequest request) throws Exception;

    // 删除购物车某件商品或者清空购物车
    String deleteShopcar(String goodId, HttpServletRequest request) throws Exception;

    // 将未登录状态的购物车转移到登录账户下
    boolean transferShopCar(String phone) throws UnknownHostException;
}

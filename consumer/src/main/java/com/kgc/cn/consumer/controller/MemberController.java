package com.kgc.cn.consumer.controller;

import com.kgc.cn.common.enums.MemberEnum;
import com.kgc.cn.common.returnResult.ReturnResult;
import com.kgc.cn.common.returnResult.ReturnResultUtils;
import com.kgc.cn.consumer.config.aop.CurrentUser;
import com.kgc.cn.consumer.config.aop.LoginRequired;
import com.kgc.cn.consumer.service.LoginAndRegisterService;
import com.kgc.cn.consumer.service.MemberService;
import com.kgc.cn.consumer.utils.redis.RedisUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * Created by Ding on 2019/12/23.
 */
@RestController
@Api(tags = "会员操作")
@RequestMapping("/member")
public class MemberController {
    @Autowired
    private MemberService memberService;
    @Autowired
    private RedisUtils redisUtils;
    @Autowired
    private LoginAndRegisterService loginAndRegisterService;

    @LoginRequired
    @ApiOperation("增加会员")
    @PostMapping("/addMember")
    public ReturnResult addMember(@CurrentUser Object o, @ApiParam("要购买或者续费的会员月份") @RequestParam String time) throws Exception {
        int addMonth = Integer.parseInt(time);
        String code = memberService.addMember(o,addMonth);
        switch (code){
            case "201":
                return ReturnResultUtils.returnFail(MemberEnum.PHONE_NULL);
            case "202":
                return ReturnResultUtils.returnFail(MemberEnum.PAY_ERROR);
            default:
                return ReturnResultUtils.returnSuccess();
        }
    }

}

package com.kgc.cn.consumer.paramModel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by boot on 2019/12/24
 */
@Data
@Builder
@ApiModel(value = "购物车显示信息model")
@AllArgsConstructor
@NoArgsConstructor
public class ShopcarInfoParam {
    @ApiModelProperty(value = "商品名字")
    private String goodName;
    @ApiModelProperty(value = "商品原价",example = "1")
    private Integer goodPrice;
    @ApiModelProperty(value = "商品折后价",example = "1")
    private double discountedPrice;
    @ApiModelProperty(value = "购买数量")
    private Integer shopNum;
    @ApiModelProperty(value = "图片地址")
    private String pictureSource;
    @ApiModelProperty(value = "商品总价",example = "1")
    private double totalPrice;
}

package com.kgc.cn.consumer.utils.Send;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;

import java.util.Random;

public class SendSms {
    public static boolean sendSms(String phoneNo, String code) {
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", "LTAI4Fh58zxoL2c94QX86zXm", "dUCu7wCBgtbKc3hSrKSrrwW7kU8LLc");
        IAcsClient client = new DefaultAcsClient(profile);
        CommonRequest request = new CommonRequest();
        request.setMethod(MethodType.POST);
        request.setDomain("dysmsapi.aliyuncs.com");
        request.setVersion("2017-05-25");
        request.setAction("SendSms");
        request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("PhoneNumbers", phoneNo);
        request.putQueryParameter("SignName", "piaoxiang");
        request.putQueryParameter("TemplateCode", "SMS_176527924");
        request.putQueryParameter("TemplateParam", "{\"code\":" + code + "}");
        try {
            CommonResponse response = client.getCommonResponse(request);
            if (response.getData() != null) {
                System.out.println(response.getData());
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static String getCode() {
        Random random = new Random();
        int code = 0;
        do {
            code = random.nextInt(10000);
        }while (code<1000);
        return code + "";
    }
}
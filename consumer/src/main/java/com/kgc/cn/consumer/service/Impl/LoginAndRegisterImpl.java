package com.kgc.cn.consumer.service.Impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.kgc.cn.common.model.dto.User;
import com.kgc.cn.common.model.dto.Weixinuser;
import com.kgc.cn.common.service.WxUserService;
import com.kgc.cn.common.utils.Date.DateUtils;
import com.kgc.cn.common.utils.http.UrlUtils;
import com.kgc.cn.consumer.service.LoginAndRegisterService;
import com.kgc.cn.consumer.service.ShopcarConsumerService;
import com.kgc.cn.consumer.utils.Send.SendSms;
import com.kgc.cn.consumer.utils.redis.RedisUtils;
import com.kgc.cn.consumer.utils.wx.WxUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

/**
 * Created by Ding on 2019/12/23.
 */
@Slf4j
@Service
public class LoginAndRegisterImpl implements LoginAndRegisterService {
    @Reference
    private WxUserService wxUserService;
    @Autowired
    private RedisUtils redisUtils;
    @Autowired
    private WxUtils wxUtils;
    @Autowired
    private ShopcarConsumerService shopcarConsumerService;

    /**
     * 根据openid查询微信用户对象
     *
     * @param openid
     * @return
     */
    @Override
    public Weixinuser queryUserByOpenId(String openid) {
        return wxUserService.queryUserByOpenId(openid);
    }

    /**
     * 新增一个微信用户对象
     *
     * @param weixinuser
     * @return
     */
    @Override
    public int insertWeiXinUser(Weixinuser weixinuser) {
        return wxUserService.insertWeiXinUser(weixinuser);
    }

    /**
     * 通过手机号查询普通用户对象
     *
     * @param phone
     * @return
     */
    @Override
    public User queryUserByPhone(String phone) {
        return wxUserService.queryUserByPhone(phone);
    }

    /**
     * 绑定手机号
     * @param o
     * @param phone
     * @param code
     * @param request
     * @return
     */
    @Override
    public String addPhone(Object o, String phone, String code,HttpServletRequest request){
        Weixinuser weixinuser = (Weixinuser) o;
        if (redisUtils.hasKey(phone)){
            if (code.equals(redisUtils.get(phone))){
                //更新微信用户的手机号
                wxUserService.updateWxUserByOpenid(weixinuser.getOpenId(),phone);
                //注册一个普通用户
                String userid = UUID.randomUUID().toString().substring(12).replaceAll("-","");
                String userName = new SimpleDateFormat("hhmmss").format(new Date()) + "wangYi" + new Random().nextInt(10);
                User user = User.builder()
                        .userId(userid)
                        .userName(userName)
                        .phone(phone)
                        .address("123")
                        .age(0)
                        .sex(3)
                        .headImgUrl("/img/.../123.jpg")
                        .build();
                wxUserService.addUser(user);
                //清除当前登录微信对象
                redisUtils.del(weixinuser.getOpenId());
                String sessionId = request.getSession().getId();
                String userJsonStr = JSONObject.toJSONString(user);
                //重新设置普通用户对象的登录状态
                redisUtils.set(sessionId, userJsonStr,60*60);
                return sessionId;
            }
            return "105";
        }
        return "104";
    }

    /**
     * 验证码登录
     * @param phone
     * @param code
     * @param request
     * @return
     * @throws UnknownHostException
     */
    @Override
    public String login(String phone, String code, HttpServletRequest request) throws Exception {
        if (redisUtils.hasKey(phone)) {
            String redisCode = redisUtils.get(phone).toString();
            User user = queryUserByPhone(phone);
            //如果用户存在
            if (null != user) {
                //验证码正确
                if (code.equals(redisCode)) {
                    String sessionId = request.getSession().getId();
                    redisUtils.set(sessionId, JSONObject.toJSONString(user), 60 * 60);
                    // 将购物车转移到登录账户下
                    shopcarConsumerService.transferShopCar(phone);
                    //删除缓存中存储的购物车
                    InetAddress ip = InetAddress.getLocalHost();
                    String ipAddress = ip.getHostAddress();
                    redisUtils.del(ipAddress);
                    //返回sessionId给前端
                    return sessionId;
                } else {
                    return "105";
                }
            } else {
                return "103";
            }
        }
        return "104";
    }

    /**
     * 登录回调
     * @param code
     * @return
     */
    @Override
    public String callBack(String code) {
        String accessJsonStr = UrlUtils.loadURL(wxUtils.getAccrssTokenUrl(code));
        JSONObject jsonObject = JSONObject.parseObject(accessJsonStr);
        String accessToken = jsonObject.getString("access_token");
        String openId = jsonObject.getString("openid");

        String UserInfoJsonStr = UrlUtils.loadURL(wxUtils.getUserInfo(accessToken, openId));
        //获得参数字符串里所需要的值
        JSONObject userInfo = JSONObject.parseObject(UserInfoJsonStr);
        int sex = Integer.parseInt(userInfo.getString("sex"));
        String nickName = userInfo.getString("nickname");
        String headImgUrl = userInfo.getString("headimgurl");
        Weixinuser weixinuser = Weixinuser.builder()
                .openId(openId)
                .loginTime(DateUtils.getSysAccurateSecond())
                .sex(sex)
                .nickName(nickName)
                .headImgUrl(headImgUrl)
                .build();
        //通过openid查询出数据库里的微信用户对象，判断有没有，没有则新增一个微信用户对象
        Weixinuser mySqlWeixinuser = queryUserByOpenId(openId);
        if (null == mySqlWeixinuser) {
            insertWeiXinUser(weixinuser);
        }
        redisUtils.set(openId, UserInfoJsonStr, 60*60);
        log.info(openId);
        return "redirect:../open.html?openid=" + openId;
    }

    /**
     * 发送验证码
     * @param phone
     * @return
     */
    @Override
    public String getCode(String phone) {
        if (redisUtils.checkSpeed("getCode", 1, 60)) {
            String code = SendSms.getCode();
            boolean flag = SendSms.sendSms(phone, code);
            if (flag) {
                redisUtils.set(phone, code, 60*5);
                return code;
            } else {
                return "102";
            }
        }
        return "101";
    }

}

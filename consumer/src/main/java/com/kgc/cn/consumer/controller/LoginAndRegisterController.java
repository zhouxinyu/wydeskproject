package com.kgc.cn.consumer.controller;

import com.kgc.cn.common.enums.LoginRegisterEnum;
import com.kgc.cn.common.returnResult.ReturnResult;
import com.kgc.cn.common.returnResult.ReturnResultUtils;
import com.kgc.cn.consumer.config.aop.CurrentUser;
import com.kgc.cn.consumer.config.aop.LoginRequired;
import com.kgc.cn.consumer.paramModel.LoginParam;
import com.kgc.cn.consumer.service.LoginAndRegisterService;
import com.kgc.cn.consumer.utils.wx.WxUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;

/**
 * Created by Ding on 2019/12/23.
 */
@Slf4j
@Controller
@Api(tags = "登录注册")
@RequestMapping("/login-register")
public class LoginAndRegisterController {
    @Autowired
    protected LoginAndRegisterService loginAndRegisterService;
    @Autowired
    private WxUtils wxUtils;

    /**
     * 微信登录
     * @return
     * @throws UnsupportedEncodingException
     */
    @ApiOperation("微信登录")
    @GetMapping("/wxLogin")
    public String wxLogin() throws Exception {
        log.info(wxUtils.getCodeUrl());
        return "redirect:../index.html?" + wxUtils.getCodeUrl();
    }

    /**
     * 微信回调
     * @param code
     * @return
     */
    @ApiOperation("微信登录回调接口")
    @RequestMapping(value = "/callBack")
    @ApiIgnore
    public String callBack(String code) {
        return loginAndRegisterService.callBack(code);
    }

    /**
     * 验证码登录
     *
     * @param loginParam
     * @param request
     * @return
     */
    @ApiOperation("登录")
    @PostMapping("/login")
    @ResponseBody
    public ReturnResult login(@Valid LoginParam loginParam, HttpServletRequest request) throws Exception {
        String code = loginAndRegisterService.login(loginParam.getPhone(), loginParam.getCode(), request);
       switch (code){
           case "105":
               return ReturnResultUtils.returnFail(LoginRegisterEnum.LOGIN_ERROR3);
           case "103":
               return ReturnResultUtils.returnFail(LoginRegisterEnum.LOGIN_ERROR1);
           case "104":
               return ReturnResultUtils.returnFail(LoginRegisterEnum.LOGIN_ERROR2);
           default:
               return ReturnResultUtils.returnSuccess(code);
       }
    }

    /**
     * 发送验证码
     *
     * @param phone
     * @return
     */
    @ApiOperation("发送验证码")
    @PostMapping("/getCode")
    @ResponseBody
    public ReturnResult getCode(String phone) {
        String code = loginAndRegisterService.getCode(phone);
        switch (code){
            case "101":
                return ReturnResultUtils.returnFail(LoginRegisterEnum.CHECK_ERROR);
            case "102":
                return ReturnResultUtils.returnFail(LoginRegisterEnum.SEND_ERROR);
            default:
                return ReturnResultUtils.returnSuccess();
        }
    }

    /**
     * 绑定手机号
     * @param o
     * @param phone
     * @param code
     * @return
     */
    @LoginRequired
    @ApiOperation("绑定手机号")
    @PostMapping("/addPhone")
    @ResponseBody
    public ReturnResult addPhone(@CurrentUser Object o,
                                 @ApiParam("手机号") @RequestParam String phone,
                                 @ApiParam("验证码") @RequestParam String code,
                                 HttpServletRequest request){
        String codeNum = loginAndRegisterService.addPhone(o,phone,code,request);
        switch (codeNum){
            case "105":
                return ReturnResultUtils.returnFail(LoginRegisterEnum.LOGIN_ERROR3);
            case "104":
                return ReturnResultUtils.returnFail(LoginRegisterEnum.LOGIN_ERROR2);
            default:
                return ReturnResultUtils.returnSuccess(codeNum);
        }
    }

}

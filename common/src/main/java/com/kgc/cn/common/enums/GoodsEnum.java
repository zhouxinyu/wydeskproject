package com.kgc.cn.common.enums;

public enum  GoodsEnum {

    VALUE_ERROR(100,"该类型商品为空"),
    GOODS_EMPTY(101,"商品不存在");

    int code;
    String msg;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    GoodsEnum(String msg) {
        this.msg = msg;
    }

    GoodsEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}

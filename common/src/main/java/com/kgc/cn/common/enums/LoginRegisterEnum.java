package com.kgc.cn.common.enums;


/**
 * Created by Ding on 2019/12/23.
 */

public enum LoginRegisterEnum {
    CHECK_ERROR(101,"请勿重复点击!"),
    SEND_ERROR(102,"验证码发送失败!"),
    LOGIN_ERROR1(103,"改手机号没有注册!"),
    LOGIN_ERROR2(104,"验证码过期，请重新获取!"),
    LOGIN_ERROR3(105,"验证码错误!");

    int code;
    String msg;

    LoginRegisterEnum() {
    }

    LoginRegisterEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}

package com.kgc.cn.provider.providerServiceImpl;

import com.alibaba.dubbo.config.annotation.Service;
import com.kgc.cn.common.model.dto.Goods;
import com.kgc.cn.common.model.dto.Promotion;
import com.kgc.cn.common.service.ProductsCommonService;
import com.kgc.cn.common.utils.Date.DateUtils;
import com.kgc.cn.provider.mapper.GoodsMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

@Service
public class ProductsProviderImpl implements ProductsCommonService {

    @Autowired
    GoodsMapper goodsMapper;

    /**
     * 商品列表要按类型来展示商品列表（从后台中获取商品列表）
     *
     * @param typeId
     * @return
     */
    @Override
    public List<Goods> ProductList(int typeId, int current, int size) throws Exception {
        int start = (current - 1) * size;
        return goodsMapper.ProductList(typeId, start, size);
    }

    /**
     * 查询该类型商品总数量
     * 用于分页
     *
     * @return
     */
    @Override
    public int queryCount(int typeId) {
        return goodsMapper.queryCount(typeId);
    }

    /**
     * 商品详情（要体现 名字 原价 实付价格 图片 描述）
     *
     * @param goodId
     * @return
     */
    @Override
    public Goods ProductsDetails(String goodId) {
        return goodsMapper.ProductsDetails(goodId);
    }

    /**
     * 商品折扣
     *
     * @param goodId
     * @return
     */
    @Override
    public int DiscountParam(String goodId) throws Exception {
        Promotion promotion = goodsMapper.DiscountParam(goodId);
        //判断商品活动是否存在
        if (null != promotion) {
            //DateUtils格式化日期
            Date timeEnd = DateUtils.accurateDay(promotion.getTimeEnd());
            Date timeStart = DateUtils.accurateDay(promotion.getTimeStart());
            Date timeNow = new Date();
            int discount = promotion.getDiscount();
            /*判断打折活动是否过期  &&  活动是否开始*/
            if (timeNow.before(timeEnd) && timeNow.after(timeStart)) {
                return discount;
            } else {
                return 100;
            }
        } else {
            return 100;
        }
    }

    /**
     * 更新某个商品id的库存
     *
     * @param goodId
     * @return
     */

    @Override
    public int ProductNum(String goodId) {
        int goodNum = goodsMapper.selectByPrimaryKey(goodId).getGoodNum();
        return goodsMapper.UpdateNum(goodNum - 1, goodId);
    }

    /**
     * 返回商品库存
     *
     * @param goodId
     * @return
     */

    @Override
    public int ProductsNum(String goodId) {
        return goodsMapper.selectByPrimaryKey(goodId).getGoodNum();
    }


}

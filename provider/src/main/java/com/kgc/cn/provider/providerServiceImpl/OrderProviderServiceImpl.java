package com.kgc.cn.provider.providerServiceImpl;

import com.alibaba.dubbo.config.annotation.Service;
import com.google.common.collect.Lists;
import com.kgc.cn.common.model.dto.Goodorder;
import com.kgc.cn.common.model.dto.GoodorderExample;
import com.kgc.cn.common.model.dto.Goods;
import com.kgc.cn.common.service.OrderCommonService;
import com.kgc.cn.common.utils.Date.DateUtils;
import com.kgc.cn.provider.mapper.GoodorderMapper;
import com.kgc.cn.provider.mapper.GoodsMapper;
import com.kgc.cn.provider.utils.OrderUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Set;

/**
 * @auther zhouxinyu
 * @data 2019/12/23
 */
@Service
public class OrderProviderServiceImpl implements OrderCommonService {
    @Autowired
    private GoodsMapper goodsMapper;
    @Autowired
    private GoodorderMapper goodorderMapper;
    @Autowired
    private OrderUtil orderUtil;

    /**
     * 生成未支付订单
     *
     * @param goodIdSet
     */
    @Override
    public List<Goodorder> noPayOrder(String Phone, int shippingId, Set<String> goodIdSet) throws Exception {
        List<Goodorder> list = Lists.newArrayList();
        String orderId = orderUtil.generateOrderId(Phone);
        goodIdSet.forEach(goodId -> {
            Goodorder goodorder = Goodorder.builder()
                    .goodId(goodId)
                    .orderId(orderId)
                    .orderTime(DateUtils.getSysAccurateSecond())
                    .phone(Phone)
                    .shippingId(shippingId)
                    .shopMoney(String.valueOf(goodsMapper.ProductsDetails(goodId).getGoodPrice()))
                    .typeId(1)
                    .build();
            list.add(goodorder);
            // 存订单
            goodorderMapper.insertSelective(goodorder);
        });
        return list;
    }

    /**
     * 通过goodid获取商品
     *
     * @param goodId
     * @return
     */

    @Override
    public Goods seleceGoodsById(String goodId) {
        return goodsMapper.selectByPrimaryKey(goodId);
    }

    /**
     * 通过订单号查找商品
     *
     * @param orderId
     * @return
     */


    @Override
    public List<Goodorder> selectgoodByOrderId(String orderId) {
        GoodorderExample goodorderExample = new GoodorderExample();
        GoodorderExample.Criteria criteria = goodorderExample.createCriteria();
        criteria.andOrderIdEqualTo(orderId);
        List<Goodorder> goodorder = goodorderMapper.selectByExample(goodorderExample);
        return goodorder;
    }

    /**
     * 更新商品订单状态
     *
     * @param orderId
     * @return
     */

    @Override
    public int updateToOrdered(String orderId) {
        return goodorderMapper.updateToOrdered(orderId);
    }

    @Autowired
    private ProductsProviderImpl productsProvider;
    /**
     * 更新商品库存
     *
     * @param orderId
     * @return
     */

    @Override
    public int updateNum(String orderId) {
        String goodId = goodorderMapper.selectByPrimaryKey(orderId).getGoodId();
        return productsProvider.ProductNum(goodId);
    }


}

package com.kgc.cn.provider.mapper;

import com.kgc.cn.common.model.dto.Goodorder;
import com.kgc.cn.common.model.dto.GoodorderExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

public interface GoodorderMapper {
    long countByExample(GoodorderExample example);

    int deleteByExample(GoodorderExample example);

    int deleteByPrimaryKey(String orderId);

    int insert(Goodorder record);

    int insertSelective(Goodorder record);

    List<Goodorder> selectByExample(GoodorderExample example);

    Goodorder selectByPrimaryKey(String orderId);

    int updateByExampleSelective(@Param("record") Goodorder record, @Param("example") GoodorderExample example);

    int updateByExample(@Param("record") Goodorder record, @Param("example") GoodorderExample example);

    int updateByPrimaryKeySelective(Goodorder record);

    int updateByPrimaryKey(Goodorder record);

    // 修改订单状态为已支付
    int updateToOrdered(String orderId);
}